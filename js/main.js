/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var stakeHolderGroupList = Array();
var groups = Array();
var person = {name: '', share: 0};
var groupDar = {value: 0, total: 0};

var defaultStakeHolderGroup = {
    name: 'Group One',
    persons: [{
            person: {
                name: 'default',
                share: 0,
                computedShare: 0
            }
        }],
    dars: [{
            dar: {
                index: 1,
                value: 1,
                total: 1
            }
        }],
    groupShare: 0
};
stakeHolderGroupList.push(defaultStakeHolderGroup);
var group = {name: 'Group One', isActive: true};
groups.push(group);
var timer = null;

var app = new Vue({
    el: '#app',
    data: {
        stakeHolderGroupList: stakeHolderGroupList,
        groups: groups,
        group: {name: '', isActive: false},
        selectedGroup: group,
        groupDar: groupDar,
        person: person,
        selectedPerson: {},
        isGlobalDar: false,
        isError: false,
        errorMessage: '',
        renderData: false,
        selectedDar: null,
        grandTotal: 0
    },
    methods: {
        addGroup: function () {
            if (!this.group.name.trim()) {
                return false;
            }

            var newStakeHolderGroup = {
                name: this.group.name,
                groupShare: 0,
                stakeHolders: [],
                dars: []
            };

            if (this.checkIfGroupExists(this.group.name)) {
                this.showError('Provided group already exists.');
                return false;
            }

            var newGroup = {
                name: this.group.name,
                isActive: false
            };

            this.stakeHolderGroupList.push(newStakeHolderGroup);
            this.groups.push(newGroup);
            this.group.name = '';
            $('#grp').focus();
            this.calculateShares();
        },
        selectGroup: function (group) {
            this.groups.forEach(function (grp) {
                grp.isActive = false;
            });
            group.isActive = true;
            this.selectedGroup = group;
        },
        removeGroup: function (group) {
            var index = -1;
            var found = this.groups.some(function (grp, i) {
                index = i;
                return grp.name === group.name;
            });

            if (found) {
                this.groups.splice(index, 1);
            }

            //Remove relevant stake holders
            index = -1;
            found = this.stakeHolderGroupList.some(function (grp, i) {
                index = i;
                return grp.name === group.name;
            });
            if (found) {
                if (this.selectedGroup.name) {
                    if (group.name === this.selectedGroup.name) {
                        this.selectedGroup.name = '';
                        this.selectedGroup.isActive = false;
                    }
                }
                this.stakeHolderGroupList.splice(index, 1);
                this.calculateShares();
            }
        },
        addDar: function () {
            var selectedGroup = this.selectedGroup;
            if (!selectedGroup) {
                this.showError('Select a group to perform this action');
                return false;
            }

            var found = this.stakeHolderGroupList.filter(function (group) {
                return group.name === selectedGroup.name;
            });


            if (found.length > 0) {
                var theGroup = found[0];
                if (!theGroup.dars) {
                    theGroup.dars = [];
                }

                var newIndex = theGroup.dars.length;
                if (newIndex === 1 && theGroup.dars[0].value === 1) {
                    var darObj = theGroup.dars[0];
                    darObj.dar.index = 1;
                    darObj.dar.value = this.groupDar.value;
                    darObj.dar.total = this.groupDar.total;
                } else {
                    theGroup.dars.push({
                        dar: {
                            index: ++newIndex,
                            value: this.groupDar.value,
                            total: this.groupDar.total
                        }
                    });
                }
                this.groupDar.value = 0;
                this.groupDar.total = 0;
                
                this.calculateShares();
            }
            $('#dar').focus();
        },
        delDar: function (dar, group) {
            this.manageDars(dar, group);
            this.calculateShares();
        },
        editDar: function (dar, group) {
            this.groupDar.value = dar.value;
            this.groupDar.total = dar.total;
            this.activateGroup(group);
            this.manageDars(dar, group);
            this.calculateShares();
            $('#dar').focus();
        },
        addPerson: function () {
            if (!this.selectedGroup.name) {
                this.showError('Select a group to perform this action');
                $('#grp').focus();
                return false;
            } else if (!$('#name').val().trim()) {
                this.showError('Enter valid name for the person');
                $('#name').focus();
                return false;
            } else if (parseFloat($('#share').val()) <= 0) {
                this.showError('Enter valid share for the person');
                $('#share').focus();
                return false;
            }

            var groupName = this.selectedGroup.name;
            if (this.checkIfPersonExists(this.person.name, this.selectedGroup)) {
                this.showError('Provided person already exists in the selected group');
                return false;
            }

            var searchResult = this.stakeHolderGroupList.filter(function (grp) {
                return grp.name === groupName;
            });

            if (searchResult.length > 0) {
                var newPerson = {
                    person: {
                        name: this.person.name,
                        share: this.person.share,
                        computedShare: 0
                    }
                };

                if (!searchResult[0].persons)
                    searchResult[0].persons = Array();

                searchResult[0].persons.push(newPerson);
                this.person.name = '';
                this.person.share = 0;
                
                this.calculateShares();
                $('#name').focus();
            }
        },
        editPerson: function (person, group) {
            this.activateGroup(group);
            this.person.name = person.name;
            this.person.share = person.share;
            this.managePersons(person, group);
            this.calculateShares();
            $('#name').focus();
        },
        delPerson: function (person, group) {
            this.managePersons(person, group);
            this.calculateShares();
        },
        showError: function (errorMsg) {
            if (timer) {
                clearTimeout(timer);
            }

            this.errorMessage = errorMsg;
            this.isError = true;
            $(".alert").show();

            timer = setTimeout(function () {
                $('.alert').fadeOut('slow');
                this.isError = false;
                this.errorMessage = '';
            }, 5000
                    );
        },
        activateGroup: function (group) {
            var searchResult = this.groups.filter(function (grp) {
                return grp.name === group.name;
            });

            this.groups.forEach(function (grp) {
                grp.isActive = false;
            });

            if (searchResult.length >= 1) {
                var foundGroup = searchResult[0];
                foundGroup.isActive = true;
                this.selectedGroup = foundGroup;
            }
        },
        manageDars: function (dar, group) {
            var index;
            var found = group.dars.some(function (dr, i) {
                index = i;
                return dr.dar.index === dar.index;
            });
            if (found) {
                group.dars.splice(index, 1);
            }
        },
        managePersons: function (person, group) {
            var index;
            var found = group.persons.some(function (p, i) {
                index = i;
                return p.person.name === person.name;
            });
            if (found) {
                group.persons.splice(index, 1);
            }
        },
        checkIfGroupExists: function (groupName) {
            if (!this.groups) {
                return false;
            }

            if (this.groups.length === 0) {
                return false;
            }

            var found = this.groups.filter(function (grp) {
                return grp.name.trim().toLowerCase() === groupName.trim().toLowerCase()
            });

            if (found.length >= 1)
                return true;
            else
                return false;
        },
        checkIfPersonExists: function (personName, group) {
            var groupsArr = this.stakeHolderGroupList.filter(function (g) {
                return g.name.trim().toLowerCase() === group.name.trim().toLowerCase();
            });

            var found = false;
            if (groupsArr.length > 0) {
                var theGroup = groupsArr[0];
                if (!theGroup.persons) {
                    found = false;
                } else {
                    found = theGroup.persons.some(function (p, i) {
                        return p.person.name.trim().toLowerCase() === personName.trim().toLowerCase();
                    });
                }
            }
            return found;
        },
        calculateShares: function () {
            var groupTotal = 0;
            var grandTotal = 0;
            
            if (this.stakeHolderGroupList.length > 0) {
                this.stakeHolderGroupList.forEach(function (holder) {
                    if (holder.dars.length) {
                        var darCounter = 1;
                        holder.dars.forEach(function (dr) {
                            var darValue = dr.dar.value;
                            var darTotal = dr.dar.total;
                            
                            //Apply on every person in the group
                            if (holder.persons) {
                                holder.persons.forEach(function (p) {
                                   var personHissa = 0;
                                   if (darCounter === 1) {
                                       personHissa = (p.person.share / darTotal) * darValue;
                                   } else {
                                       personHissa = (p.person.computedShare / darTotal) * darValue;
                                   }
                                   p.person.computedShare = personHissa;
                                });
                            }
                            ++darCounter;
                        });
                        
                        //Calculate group total share
                        if (holder.persons) {
                            groupTotal = 0;
                            holder.persons.forEach(function (p) {
                               groupTotal += p.person.computedShare; 
                            });
                            holder.groupShare = groupTotal;
                            grandTotal += holder.groupShare;
                        }
                    }
                });
                
                this.grandTotal = grandTotal;
            }
        },
        resetEnv: function () {
            this.grandTotal = 0;
            this.groups.length = 0;
            this.selectedGroup.name = '';
            this.selectedGroup.isActive = false;
            this.stakeHolderGroupList.length = 0;
            app.$forceUpdate();
        }
    }
});

$(function() {
   $('#name').focus(); 
});




